"""
Command line interface for the UAV RGB Ortho PLots Extraction All module.
"""
import argparse
import logging
import sys
import os
import time

from module import uav_rgb_orthoplotsextraction_all
from module import utils
from module import error

PROCESS_NAME = "UAV RGB Ortho PLots Extraction"


def create_parser():
	"""
	Create the parser for the imageselection-av-rgbi command.

	:return: Configured parser.
	:rtype: argparse.ArgumentParser
	"""
	parser = argparse.ArgumentParser(
		description="extract each plot from the orthomosaic tiff file"
	)

	# Positional arguments, declaration order is important
	parser.add_argument(
		'output_folder',
		help="path to the directory where the output will be generated"
	)

	# Positional arguments, declaration order is important
	parser.add_argument(
		'ortho_path',
		help='path to the orthomosaic of the experiment'
	)

	parser.add_argument(
		'geojson_path',
		help='path to the geojson file containing the GPS cutting of the plots'
	)

	parser.add_argument(
		"-d",
		"--datetime",
		help="date and time of the data (format: dd/MM/YYYY HH:MM:SS)",
		type=utils.valid_date)

	parser.add_argument(
		"-v",
		"--verbose",
		help="activate output verbosity",
		action="store_true",
		default=False)

	return parser.parse_args()


if __name__ == '__main__':
	timer_start = time.time()
	try:
		# Manage CLI arguments
		args = create_parser()

		# Init Logger
		logger = logging.getLogger()

		if args.verbose:
			logger.setLevel(logging.DEBUG)
		else:
			logger.setLevel(logging.INFO)

		logger.addHandler(logging.StreamHandler(sys.stdout))
		logger.addHandler(logging.FileHandler(os.path.splitext(__file__)[0] + '.log', 'w'))

		logging.debug("CLI arguments are valid")

		# Creating output folder if doesn't exist
		if not os.path.exists(args.output_folder):
			logging.debug("Create output folder '" + args.output_folder + "'")
			os.mkdir(args.output_folder)

		# Run module core process
		uav_rgb_orthoplotsextraction_all.run(args.ortho_path, args.geojson_path, args.output_folder)

		logging.info("Process '" + PROCESS_NAME + "' complete")

	except error.DataError as e:
		# When DataError is raised, the process fails without killing the overall execution
		logging.exception(e)
		logging.error("Process '" + PROCESS_NAME + "' failed")
	except Exception as e:
		# Every other errors kill the overall execution
		logging.exception(e)
		logging.error("Process '" + PROCESS_NAME + "' failed")
		sys.exit(1)
	finally:
		timer_end = time.time()
		logging.debug("Process '" + PROCESS_NAME + "' took " + str(int(timer_end - timer_start)) + " seconds")

