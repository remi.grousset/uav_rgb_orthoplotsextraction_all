from pathlib import Path
import cv2
import geojson
import tifffile as tiff
import numpy as np

from module import utils


def save_data(dst, out_dir, resolution, plot_name):
    """
    Writes the image of the plot as tif file
    return that

    :param dst: ndarray containing the image to save
    :param out_dir: folder where to save the plot
    :param resolution: resolution in mm/pixel
    :param plot_name: str containing the plot name
    :return:
    """
    Path(out_dir).parent.mkdir(parents=True, exist_ok=True)
    Path(out_dir).mkdir(parents=True, exist_ok=True)
    resolution_str = str(int(resolution)) + "mm" + str(resolution)[2: 4]
    name = out_dir + "\\" + plot_name + "_" + resolution_str + ".tif"
    cv2.imwrite(name, dst)


def split_orthomosaic(tiff_file, geojson_file, out_dir):
    """
    splits the orthomosaic into tif images of each plot
    :param tiff_file: WindowsPath to access the orthomosaic file
           geojson_file: WindowsPath to acces the geojson file containing the GPS position of each plot
    """
    with open(geojson_file) as f:
        coords = geojson.load(f)

    bBox_GPS = utils.load_wgs_84_bbox(tiff_file)
    ortho = tiff.imread(tiff_file)
    bBox_PIX = ((0, 0), (ortho.shape[0], ortho.shape[1]))
    for feature in coords["features"]:
        if 'Alias' in feature["properties"].keys():
            plot_name = feature["properties"]['Alias']
        elif 'Nom' in feature["properties"].keys():
            plot_name = feature["properties"]['Nom']
        else:
            plot_name = 'No_file_name_found'

        print(plot_name)
        area_box = utils.get_area_box(feature)
        coords_PIX = utils.convert_from_gps_to_pix(area_box, bBox_GPS, bBox_PIX)
        croped = utils.crop_bounding_rect(coords_PIX, ortho)
        dst, area_pix = utils.mask_borders(coords_PIX, croped)
        resolution = utils.calc_res_pix2meters(area_pix, area_box) * 1000
        save_data(np.rot90(dst[:, :, ::-1], axes=(0, 1)), str(out_dir), resolution, plot_name)


def run(orthomosaic, geojson, output_folder):
    """
    run the module
    :param orthomosaic: str: path to the orthomosaic
    :param geojson: str: path to the geojson
    :param output_folder: str: path to the output folder where to write the results
    """
    split_orthomosaic(Path(orthomosaic), Path(geojson), Path(output_folder))
