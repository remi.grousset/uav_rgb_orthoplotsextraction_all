import argparse
from datetime import datetime
from geotiff import GeoTiff
from math import sin, cos, acos, pi
import numpy as np
import cv2

DATETIME_FORMAT = "%d/%m/%Y %H:%M:%S"


def valid_date(s):
    """Validate the datetime given in CLI"""
    try:
        return datetime.strptime(s, DATETIME_FORMAT)
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def dms2dd(d, m, s):
    """Convertit un angle "degrés minutes secondes" en "degrés décimaux"
    """
    return d + m / 60 + s / 3600


def dd2dms(dd):
    """Convertit un angle "degrés décimaux" en "degrés minutes secondes"
    """
    d = int(dd)
    x = (dd - d) * 60
    m = int(x)
    s = (x - m) * 60
    return d, m, s


def deg2rad(dd):
    """Convertit un angle "degrés décimaux" en "radians"
    """
    return dd / 180 * pi


def rad2deg(rd):
    """Convertit un angle "radians" en "degrés décimaux"
    """
    return rd / pi * 180


def distanceGPS(latA, longA, latB, longB):
    """Retourne la distance en mètres entre les 2 points A et B connus grâce à
       leurs coordonnées GPS (en radians).
    """
    # Rayon de la terre en mètres (sphère IAG-GRS80)
    RT = 6378137
    # angle en radians entre les 2 points
    S = acos(sin(deg2rad(latA)) * sin(deg2rad(latB)) + cos(deg2rad(latA)) *
             cos(deg2rad(latB)) * cos(abs(deg2rad(longB) - deg2rad(longA))))
    # distance entre les 2 points, comptée sur un arc de grand cercle
    return S * RT


def calc_res_pix2meters(area_pix, area_box):
    """calculate the dimension of a pixel in meter
    return the resolution
    :param area_pix:area of a box in pixel
           area_box: area of that box in meter square
    :return the resolution of the image
    """
    area_meters = (distanceGPS(area_box[0][1], area_box[0][0], area_box[1][1], area_box[1][0]) *
                   distanceGPS(area_box[1][1], area_box[1][0], area_box[2][1], area_box[2][0]))
    return np.sqrt(area_meters / area_pix)


def convert_from_gps_to_pix(coords_gps, bbox_gps, bbox_pix):
    """
    Converts gps coordinates to pixel coordinates (in a geotiff) from the bounding box of that geotiff in GPS coordinates and pixel coordinates
    :param coords_gps: list containing de GPS coordinates to convert
           bbox_gps: list containing the GPS coordinates of the corners of the bounding box of the geotiff
           bbox_pix: list containing the pixels coordinates of the corners of the bounding box of the geotiff

    """
    a_x = (bbox_pix[1][0] - bbox_pix[0][0]) / (bbox_gps[1][1] - bbox_gps[0][1])
    b_x = - a_x * bbox_gps[0][1]
    a_y = (bbox_pix[1][1] - bbox_pix[0][1]) / (bbox_gps[1][0] - bbox_gps[0][0])
    b_y = - a_y * bbox_gps[0][0]
    coords_pix = []
    for coord in coords_gps:
        coords_pix.append([int(a_y * coord[0] + b_y), int(a_x * coord[1] + b_x)])

    return np.array(coords_pix)


def load_wgs_84_bbox(tiff_file):
    """
    extracts the bounding box from a geotiff
    :param tiff_file: WindowsPath to access the geotiff file
    :return a list containing the coordinates of the bounding box
    """
    geotiff = GeoTiff(tiff_file)
    bBox_GPS = geotiff.tif_bBox_wgs_84
    del geotiff
    return bBox_GPS


def get_area_box(feature):
    """
    extracts the area box from the features and return the area as a list of tuples
    :param feature: dict containing the features
    :return list of tuples containing the coordinates of the corners of the microplot
    """
    return [(feature["geometry"]["coordinates"][0][1][0],
             feature["geometry"]["coordinates"][0][1][1]),
            (feature["geometry"]["coordinates"][0][2][0],
             feature["geometry"]["coordinates"][0][2][1]),
            (feature["geometry"]["coordinates"][0][3][0],
             feature["geometry"]["coordinates"][0][3][1]),
            (feature["geometry"]["coordinates"][0][4][0],
             feature["geometry"]["coordinates"][0][4][1])]


def crop_bounding_rect(coords_PIX, ortho):
    """
    crops the orthomosaic to extract the minimum rectangle containing the plot diescribed by the pixel coordinates
    return the cropped rectangle
    :param coords_PIX: ndarray containing the coordinates in pixel of the corners of the microplot
           ortho: ndarray containing the orthomosaic image
    :return ndarray containing the extracted rectangle
    """
    rect = cv2.boundingRect(coords_PIX)
    x, y, w, h = rect
    return ortho[y:y + h, x:x + w].copy()


def mask_borders(coords_PIX, croped):
    """
    masks all the pixels out of the microplot
    return the masked image
    :param coords_PIX: ndarray containing the coordinates in pixel of the corners of the microplot
           croped: ndarray containing the image to mask
    :return ndarray containing the masked image
    """
    pts = coords_PIX - coords_PIX.min(axis=0)
    mask = np.zeros(croped.shape[:2], np.uint8)
    cv2.drawContours(mask, [pts], -1, (255, 255, 255), -1, cv2.LINE_AA)
    indexes = np.where(mask == 255)
    return cv2.bitwise_and(croped, croped, mask=mask)[:, :, 0: 3], indexes[0].shape[0]
