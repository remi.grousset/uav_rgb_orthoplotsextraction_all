import unittest
from argparse import ArgumentTypeError

from module import utils


class TestUtils(unittest.TestCase):
    """
    Test case for utils methods

    TODO: Add other test cases. For help refer to this tutorial: https://docs.python.org/3/library/unittest.html
    """
    def test_valid_date(self):

        # Test KO
        with self.assertRaises(ArgumentTypeError):
            utils.valid_date("21-05-2015 15:25:35")

        # Test OK
        self.assertIsNotNone(utils.valid_date("21/05/2015 15:25:35"))


if __name__ == '__main__':
    unittest.main()
